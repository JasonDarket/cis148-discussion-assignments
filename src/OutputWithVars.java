/*
   Name: (Enter your name here)
   Date: (Enter date completed here)
*/

import java.util.Scanner; 

public class OutputWithVars {
   public static void main(String[] args) {
      Scanner scnr = new Scanner(System.in);
      int userNum;
      int userNum2;
      int userNumCalc;

      System.out.println("Enter integer:");
      userNum = scnr.nextInt();      
      
      System.out.println("You entered " + userNum);
      
      userNumCalc = userNum * userNum;
      System.out.println(userNum + " squared is " + userNumCalc);
      
      userNumCalc = userNum * userNum * userNum;
      System.out.println(userNum + " cubed is " + userNumCalc);
      
      System.out.println("Enter another integer:");
      userNum2 = scnr.nextInt();
      
      userNumCalc = userNum + userNum2;
      System.out.print(userNum + " + ");
      System.out.println(userNum2 + " is " + userNumCalc);
      
      userNumCalc = userNum * userNum2;
      System.out.print(userNum + " * ");
      System.out.println(userNum2 + " is " + userNumCalc);
      
      
   }
}