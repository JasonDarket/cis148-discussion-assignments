/*
   Name: Greg Dillon
   Date: 9/16/18
*/

import java.util.Scanner;

public class BasicInput {
   public static void main(String[] args) {
      Scanner scnr = new Scanner(System.in);
      int userInt;
      int castInt;
      double userDouble;
      char userChar;
      String userString;
    
      System.out.println("Enter an integer:");
      userInt = scnr.nextInt();
      
      System.out.println("Enter a double:");
      userDouble = scnr.nextDouble();
      
      System.out.println("Enter a character:");
      userChar = scnr.next().charAt(0);
      
      System.out.println("Enter a string:");
      userString = scnr.next();
      
      System.out.println(userInt + " " + userDouble + " " + userChar + " " + userString);

      System.out.println(userString + " " + userChar + " " + userDouble + " " + userInt);
      
      castInt = (int)userDouble;
      System.out.print(userDouble + " cast to an integer is " + castInt);
   }
}