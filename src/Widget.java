/*
    Name: Greg Dillon
    Date: 2018-12-1


    This application will demonstrate the creation of a collection of objects.
    There will be user input to create individual widget objects that contain
    a name field and a price.

    Team member with branch: No Team Members
 */


public class Widget {
    // Object fields (object variables, members, etc)
    private String name;
    private double price;

    // Class fields (class variables that are created even without any objects created)
    private final static int MAX_WIDGETS = 5;
    private static int count = 0;

    //Argument-based Constructor
    public Widget(String name, double price) {
        this.name = name;
        this.price = price;
    }

    //No-argument constructor
    public Widget() {
        this("Generic", 1.0);
    }

    //Object methods
    public String getName() { return name; }
    public double getPrice() { return price; }
    public void setPrice() {}

    //Class methods
    public static int getMaxWidgets() { return MAX_WIDGETS; }
    public static int getCount() { return count; }
    public static void updateCount() { count++; }

    //Override methods
    public String toString() {
        return "Name: " + name + "Price: " + price;
    }
}
